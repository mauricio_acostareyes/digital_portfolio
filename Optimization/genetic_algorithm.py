# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 18:35:54 2021

@author: mauri
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import pandas as pd
import random

class GA:
    def __init__(self,obj_fxn,N_pop,N_var,constraints,selection_rate,mutation_rate):
        df=pd.DataFrame()
        for i in range(N_var):
            df[f'x{i}']=np.random.random(N_pop)*(constraints[i]['max']-constraints[i]['min'])+constraints[i]['min']
        df['cost']=[obj_fxn(x) for _,x in df.iterrows()]
        df.sort_values(by='cost',inplace=True,ignore_index=True)
        self.df=df
        self.obj_fxn=obj_fxn
        self.selection_rate=selection_rate
        if np.round(N_pop*selection_rate)%2!=0:
            self.num_selected=int(np.round(N_pop*selection_rate))
        else:
            self.num_selected=int(np.round(N_pop*selection_rate))-1
        self.mutation_rate=mutation_rate
        num_mut=int(np.ceil(mutation_rate*N_var*(N_pop-1)))
        self.num_mutations=num_mut
        self.N_pop=N_pop
        self.N_var=N_var
        self.constraints=constraints
        self.generations=[df]
        #rank weighting
        N_keep=int(self.N_pop*self.selection_rate)
        if N_keep%2!=0:
            N_keep+=1
        cumulative_P=[]
        prev=0
        for rank in range(1,N_keep+1):
            cumulative_P.append(prev+(N_keep-rank+1)/np.sum([x for x in range(1,N_keep+1)]))
            prev=cumulative_P[rank-1]
        cumulative_P=np.round(cumulative_P,2)
        self.cumulative_P=cumulative_P
    def selection(self):
        parents=[]
        while len(parents)<self.num_selected/2:
            m1=d1=0
            while m1==d1:
                m1,d1=random.choices([x for x in range(len(self.cumulative_P))],cum_weights=self.cumulative_P,k=2)
            parents.append((m1,d1))
        m2=d2=0
        while m2==d2:
            m2,d2=random.choices([x for x in range(len(self.cumulative_P))],cum_weights=self.cumulative_P,k=2)
        return parents
    def crossover(self):
        all_parents=self.selection()
        children=[]
        while len(all_parents)>0:
            parents=all_parents.pop()
            m1,d1=parents
            m1=list(self.df.iloc[m1,:self.N_var])
            d1=list(self.df.iloc[d1,:self.N_var])

            kinetochore=random.choices([x for x in range(self.N_var)],k=1)[0]
            beta=np.random.rand()
            new11=m1[kinetochore]-beta*(m1[kinetochore]-d1[kinetochore])
            new12=d1[kinetochore]+beta*(m1[kinetochore]-d1[kinetochore])
            if kinetochore==0:
                new11=[new11]+d1[kinetochore+1:]
                new12=[new12]+m1[kinetochore+1:]
            elif kinetochore==self.N_var-1:
                new11=m1[:kinetochore]+[new11]    
                new12=d1[:kinetochore]+[new12]
            else:
                new11=m1[:kinetochore]+[new11]+d1[kinetochore+1:]
                new11=d1[:kinetochore]+[new12]+m1[kinetochore+1:]
            new11+=[self.obj_fxn(new11)]
            new12+=[self.obj_fxn(new12)]
            children.append(new11)
            children.append(new12)

        
        df_children=pd.DataFrame(data=children,columns=self.df.columns)
        self.df=self.df.iloc[:len(self.cumulative_P),:].append(df_children).sort_values(by='cost',ignore_index=True)
    
    def mutation(self):
        mutate=[]
        while len(mutate)<self.num_mutations:
            obs=random.choices([x for x in range(1,self.N_pop)],k=1)[0]
            variable=random.choices([x for x in range(self.N_var)])[0]
            if [obs,variable] not in mutate:
                mutate.append([obs,variable])
        for obs,variable in mutate:
            self.df.iloc[obs,variable]=np.random.rand()*(self.constraints[variable]['max']-self.constraints[variable]['min'])+self.constraints[variable]['min']
        new_costs=[]
        for index,row in self.df.iloc[:,:-1].iterrows():
            new_costs.append(self.obj_fxn(row))
        self.df['cost']=new_costs
        self.df.sort_values(by='cost',inplace=True,ignore_index=True)
    
    def fmin(self,plot=False,num_iterations=100,cmap=cm.inferno):
        
        for _ in range(num_iterations):
            self.crossover()
            self.mutation()
            self.generations.append(self.df)
        
        if plot:
            if self.N_var!=2:
                print('Can only plot for 2D objective fxn')
            else:
                x=np.linspace(self.constraints[0]['min'],self.constraints[0]['max'],1000)
                y=np.linspace(self.constraints[1]['min'],self.constraints[1]['max'],1000)
                X,Y=np.meshgrid(x,y)
                
                XY=np.array([np.ravel(X),np.ravel(Y)]).T
                Z=np.array(list(map(self.obj_fxn,XY))).reshape(np.shape(X))
                fig=plt.figure()
                ax1=fig.add_subplot(1,2,1,projection='3d')
                ax2=fig.add_subplot(2,2,2)
                surf=ax1.plot_surface(X,Y,Z,cmap= cmap,antialiased=True)
                ax1.set_xlabel('x1',size=15)
                ax1.set_ylabel('x2',size=15)
                ax1.set_zlabel('cost',size=15)
                fig.colorbar(surf,label='cost')
                
                cont=ax2.contourf(X,Y,Z,cmap=cmap,alpha=.9)
                ax2.set_xlabel('x1',size=15)
                ax2.set_ylabel('x2',size=15)
                
                min_cost=np.min(Z)
                z=np.array([np.ravel(X),np.ravel(Y),np.ravel(Z)]).T
                for xx,yy,zz in z:
                    if zz==min_cost:
                        break
                ax2.text(xx,yy,'Global Min\n',color='white',horizontalalignment='center')
                
                avg_cost=[];
                min_cost=[];
                x_min=[]
                y_min=[]
                for df in self.generations:
                    avg_cost.append(df.mean()['cost'])
                    min_cost.append(df.min()['cost'])
                    x_c,y_c=df.iloc[0,:-1]
                    x_min.append(x_c)
                    y_min.append(y_c)
                ax2.plot(x_min,y_min,ls='-',marker='o',lw=1,color='blue',label='Best solution\neach generation')
                ax2.text(x_min[0],y_min[0],'Start',horizontalalignment='center',verticalalignment='bottom',color='white')
                ax2.text(x_min[-1],y_min[-1],'Finish',horizontalalignment='center',verticalalignment='bottom',color='white')
                ax2.legend(bbox_to_anchor=(1,1),loc='lower left')
                ax3=fig.add_subplot(2,2,4)
                ax3.plot([x for x in range(len(min_cost))],min_cost,label='minimum')
                ax3.plot([x for x in range(len(avg_cost))],avg_cost,label='average')
                ax3.axhline(np.min(Z),label='Global min',c='r',ls='--',lw=1)
                ax3.legend()
                ax3.set_xlabel('Generation')
                ax3.set_ylabel('Cost')
                
                fig.subplots_adjust(top=0.88,
                                    bottom=0.11,
                                    left=0.11,
                                    right=0.9,
                                    hspace=0.4,
                                    wspace=0.2)
        print(f'Min Value after {num_iterations} generations = {self.df.iloc[0,-1]:.2f}')
        return self.df.iloc[0,-1]
    
plt.close('all')

color=cm.inferno
Obj_fxn=lambda x: x[0]*np.sin(4*x[0])+1.1*x[1]*np.sin(2*x[1])

np.random.seed(245)       
        
a=GA(Obj_fxn,8,2,[{"min":1,"max":10},{"min":2,"max":8}],.5,.3)
a.fmin(plot=True,num_iterations=50)


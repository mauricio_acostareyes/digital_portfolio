# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 07:19:29 2021

@author: mau22560
"""

import numpy as np
import scipy as sci
import matplotlib.pyplot as plt
import pandas as pd
import copy
import matplotlib

class Nelder_Mead:
    def min_max_centroid(self):
        x_l=self.simplex[0]
        x_h=self.simplex[0]
        x_o=None#centroid
        
        for x in self.simplex[1:]:
            if self.obj_fxn(x)<self.obj_fxn(x_l):
                x_l=x
            elif self.obj_fxn(x)>self.obj_fxn(x_h):
                x_h=x
        x_o=np.sum([x for x in self.simplex if np.all(x!=x_h)],axis=0)/len(self.simplex[0])
        self.x_l=x_l
        self.x_h=x_h
        self.x_o=tuple(x_o)
    def __init__(self,simplex,obj_fxn,alpha=1,beta=.5,gamma=2,sigma=.5):
        self.simplex=copy.deepcopy(simplex)
        self.alpha=alpha
        self.beta=beta
        self.gamma=gamma
        self.sigma=sigma
        self.obj_fxn=obj_fxn
        self.min_max_centroid()

    def create_simplex(self,c,n,regular=False):
        vertices=[]
        if not regular:
            for i in range(n):
                v=n*[0]
                v[i]=c
                vertices.append(v)
            vertices.append(n*[0])
        else:
            b=c/(n*(2**.5))*((n+1)**.5 +n -1)
            a=b-c/(2**.5)
            for i in range(n):
                v=n*[a]
                v[i]=b
                vertices.append(v)
            vertices.append(n*[0])
        self.simplex=vertices
    
    def fminsearch(self,max_iterations=1000,min_std=.00001,plot=False,plot_frequency=1):
        self.iteration=1
        self.std=1
        print('\n\nInitial Simplex = ',self.simplex)
        if plot:
            greys=matplotlib.cm.Greys
            colors=['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
            fig,ax=plt.subplots(figsize=(10,10))
            ax.add_patch(matplotlib.patches.Polygon(np.reshape(self.simplex,(3,2)),fc='k'))#greys(.25)))
            S=self.simplex+[self.simplex[0]]
            ax.plot(*np.hsplit(np.array(S),2),label=f'Iteration {self.iteration}',color='None')
        while self.iteration<=max_iterations and self.std>min_std:
            #update x_l,x_h,x_o
            self.min_max_centroid()
            print('\n')
            print(f'Iteration {self.iteration}')
            fxl=self.obj_fxn(self.x_l)
            self.fxl=fxl
            fxh=self.obj_fxn(self.x_h)
            self.fxh=fxh
            self.x_r=tuple(np.array(self.x_o)+self.alpha*(np.array(self.x_o)-np.array(self.x_h)))
            fxr=self.obj_fxn(self.x_r)
            self.fxr=fxr
            if fxr<fxh and fxr>=fxl:
                print("Successful reflection")
                for index in range(len(self.simplex)):
                    if np.all(self.simplex[index]==self.x_h):
                        self.simplex[index]=self.x_r
                        break  
            elif fxr<fxl:
                print("Expansion")
                self.x_e=tuple(np.array(self.x_o)+self.gamma*(np.array(self.x_r)-np.array(self.x_o)))
                fxe=self.obj_fxn(self.x_e)
                self.fxe=fxe
                if fxe<fxr:
                    print("Expansion Successful")
                    
                    for index in range(len(self.simplex)):
                        if np.all(self.simplex[index]==self.x_h):
                            self.simplex[index]=self.x_e
                            break  
                else:
                    print("Expansion Unsuccessful")
                    for index in range(len(self.simplex)):
                        if np.all(self.simplex[index]==self.x_h):
                            self.simplex[index]=self.x_r
                            break  
            elif fxr>=fxh:
                print("Contraction")
                
                self.x_c=tuple(self.beta*np.array(self.x_h)+(1-self.beta)*np.array(self.x_o))
                fxc=self.obj_fxn(self.x_c)
                self.fxc=fxc
                if fxc<fxh:
                    print("Contraction Successfull")
                    
                    for index in range(len(self.simplex)):
                        if np.all(self.simplex[index]==self.x_h):
                            self.simplex[index]=self.x_c
                            break
                else:
                    print("Contraction Unsuccessful")
                    print("Scaling")
                    
                    new_vertices=[self.x_l]
                    for old_v in self.simplex:
                        if np.all(old_v!=self.x_l):
                            new_vertices.append(tuple(np.array(self.x_l)+self.sigma*(np.array(old_v)-np.array(self.x_l))))
                    self.simplex=new_vertices
            self.std=np.std([self.obj_fxn(x) for x in self.simplex])
            print(self.simplex)
            if plot:
                ax.add_patch(matplotlib.patches.Polygon(np.reshape(self.simplex,(3,2)),fc=colors[self.iteration%len(colors)]))
                S=self.simplex+[self.simplex[0]]
                ax.plot(*np.hsplit(np.array(S),2),label=f'Iteration {self.iteration}',color='None')
            self.iteration+=1
        if self.iteration<max_iterations:
            print("\nAlgorithm Converged to local minimum")
            return self.simplex,np.min([self.obj_fxn(x) for x in self.simplex])
        else:
            print("Max iterations reached")

simplex=[(2,-3),(12,10),(-3,8)]
obj_fxn=lambda x: x[0]**2 +x[1]**2 -x[0]*x[1]-7*x[0]-4*x[1]
a=Nelder_Mead(simplex,obj_fxn)
solution=a.fminsearch(max_iterations=30,plot=True)